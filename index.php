<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <title>PHP TESTER</title>
  <style>
    .container {
      max-width: 550px;
    }
  </style>
</head>

<body>
  <div class="container mt-5">
    <h3>Data Insert</h3>
    <a href="view_data.php" class="btn btn-sm btn-primary">Viewer</a>
    <hr>

    <form action="view_data.php" method="post">
      <div class="form-group">
        <label for="">First Name</label>
        <input class="form-control" name="first_name" type="text" id="txtFirstName">
      </div>
      <div class="form-group">
        <label for="">Last Name</label>
        <input class="form-control" name="last_name" type="text" id="txtLastName">
      </div>

      <div class="form-group">
        <button class="btn btn-primary btn-block" type="submit" id="saveData">Save</button>
        <script type="text/javascript">
          $(document).ready(function () {
            $("#saveData").click(function () {
              console.log("Insert data")
              var fname = $("#txtFirstName").val();
              var lname = $("#txtLastName").val();

              if (fname == '' || lname == '') {
                return false;
              }

              $.ajax({
                type: "POST",
                url: "services.php",
                dataType: "JSON",
                data: {
                  first_name: fname,
                  last_name: lname
                },
                cache: false,
                success: function (data) {
                  alert(data);
                },
                error: function (xhr, status, error) {
                  console.error(xhr);
                }
              })
            });
          });

        </script>
      </div>
    </form>
  </div>
</body>

</html>