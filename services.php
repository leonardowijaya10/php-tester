<?php
$url = "localhost:3306";
$database = "php-tester";
$username = "root";
$password = "";

$conn = mysqli_connect($url, $username, $password, $database);
if (!$conn) {
    die("Unable to connect: " . $conn->connect_error);
}

$method = $_SERVER['REQUEST_METHOD'];
if ($method == "GET") {
    $sql = "SELECT * FROM tb_user";
    $results = mysqli_query($conn, $sql);

    $rows = array();
    if (mysqli_num_rows($results) > 0) {
        while ($record = mysqli_fetch_assoc($results)) {
            array_push($rows, $record);
        }
        print json_encode($rows);
    } else {
        echo "No records found";
    }
} else if ($method == "POST") {
    $fname = $_POST['first_name'];
    $lname = $_POST['last_name'];

    $sql_insert = "INSERT INTO tb_user(first_name, last_name) VALUES('$fname','$lname')";

    if (mysqli_query($conn, $sql_insert)) {
        echo "Data successfully inserted";
    } else {
        echo "Error: " . mysqli_error($conn);
    }
}
$conn->close();
?>