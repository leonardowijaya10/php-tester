<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <title>PHP TESTER</title>
</head>

<body>
    <div class="container mt-5">
        <h3>Data List</h3>
        <a href="index.php" class="btn btn-sm btn-primary">Back to Index</a>
        <br>
        <table class="table table-striped table-hover" id="datas">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $url = "localhost:3306";
                $database = "php-tester";
                $username = "root";
                $password = "";

                $conn = mysqli_connect($url, $username, $password, $database);
                if (!$conn) {
                    die("Unable to connect: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tb_user";
                $results = mysqli_query($conn, $sql);
                if (mysqli_num_rows($results) > 0) {
                    while ($record = mysqli_fetch_array($results)) {
                        echo "<tr><td>" . ($record['id']) . "</td>";
                        echo "<td>" . ($record['first_name']) . "</td>";
                        echo "<td>" . ($record['last_name']) . "</td></tr>";
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</body>

</html>